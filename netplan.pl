#!/usr/bin/perl
use warnings;
use File::Slurp;
use Getopt::Long;

my $version = "6";

my $gateway_ipv4;
my $gateway_ipv6;
GetOptions("gw4=s" => \$gateway_ipv4, "gw6=s" => \$gateway_ipv6) || die "Error in arguments";

@all_ifs = qx/ip link show 2>&1/;

my $out =<<"HERE";
network:
  version: 2
  ethernets:
HERE

my @dhcp_ifs = ();
foreach my $line (@all_ifs)
{
	next unless $line =~ /^\d+:\s+([\d\w]+):/;
	my $if = $1;
	next unless $if =~ /^ens/;
	#~ next if $if =~ /lo/;
	push(@dhcp_ifs, $if);
}
if(scalar(@dhcp_ifs) < 1)
{
	print STDERR "ERROR: No interfaces suitable for DHCP found";
	exit 1;
}
elsif(scalar(@dhcp_ifs) > 1)
{	# This is a firewall instance
	for(my $i = 0; $i < scalar(@dhcp_ifs); $i++)
	{
		my $if = $dhcp_ifs[$i];
	
		$out .= <<"HERE";
    $if:
      dhcp4: true
      dhcp6: true
HERE
		if($i > 0)
		{
			$out .= <<"HERE";
      dhcp4-overrides:
        use-routes: false
      dhcp6-overrides:
        use-routes: false
HERE
		}
		else
		{
			$out .= <<"HERE";
      routes:
        - to: ::/0
          via: FE80:EC2::1
          metric: 50
HERE
		}
	}
}
else
{	# This is an internal instance
	$out .= <<"HERE";
    ens5:
      dhcp4: true
      dhcp6: true
      dhcp4-overrides:
        use-routes: false
      dhcp6-overrides:
        use-routes: false
      routes:
        - to: 0.0.0.0/0
          via: ${gateway_ipv4}
        - to: ::/0
          via: ${gateway_ipv6}
          metric: 50
HERE
}

print $out."\n"; 

