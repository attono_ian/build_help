#!/usr/bin/perl
use strict;
use warnings;
use Paws;
use Getopt::Long;

my $quiet;
unless(GetOptions('q|quiet' => \$quiet))
{
	print STDERR "Bad options\n";
	exit 3;
}

my $instance_id = `cloud-init query v1.instance_id`; chomp $instance_id;
my $region = `cloud-init query region`; chomp $region;
my $ec2 = Paws->service('EC2',
  region => $region,
);
print "Instance ID:$instance_id Region:$region\n" unless $quiet;
my $result = $ec2->DescribeInstances(InstanceIds => [$instance_id]);
unless(scalar(@{$result->Reservations()}) == 1)
{
	print STDERR "Didn't get exactly one result when searching for myself\n";
	exit 1;
}
my $instances = ${$result->Reservations()}[0]->Instances();
unless(scalar(@$instances) == 1)
{
	print STDERR "Reservation doesn't contain exactly one instance\n";
	exit 2;
}
my $instance = $$instances[0];
#print "Current instance tags:\n";
#foreach my $tag (@{$instance->Tags()})
#{
	#	print "\t".$tag->Key.": ".$tag->Value."\n";
#}
my $request_id = $instance->SpotInstanceRequestId;
unless($request_id)
{
	print "I don't have a spot request ID; I am probably not a spot instance. Doing nothing.\n" unless $quiet;
	exit 0;
}
print "Spot Request ID: $request_id\n" unless $quiet;
my $requests = $ec2->DescribeSpotInstanceRequests('SpotInstanceRequestIds' => [$request_id]);
unless(scalar(@{$requests->SpotInstanceRequests}) == 1)
{
	print STDERR "Spot request doesn't contain exactly one request\n";
	exit 4;
}
my $request = ${$requests->SpotInstanceRequests}[0];
print "Deleting all current tags on instance..." unless $quiet;
$ec2->DeleteTags(Resources => [$instance_id], Tags => $instance->Tags);
print "done\n" unless $quiet;
print "Copying tags from request to instance..." unless $quiet;
$ec2->CreateTags(Resources => [$instance_id], Tags => $request->Tags);
print "done\n" unless $quiet;
print "Tags now set to:\n" unless $quiet;
foreach my $tag (@{$request->Tags()})
{
	#p $tag;
	print "\t".$tag->Key.": ".$tag->Value."\n" unless $quiet;
}

